1. The initial.sql contains script for database creation.
Then in scripts for liquibase in driver-account-core\build.gradle
in task for liquibase ('dev') you should set user and password (instead of my_user/my_password) that has credentials for creation roles, and tables.
When application starts the scripts apply.

To manually apply liquibase script run in console gradlew task dev update.
In case of change something in scripts run previously in console gradlew task dev clearChecksum.

There is no any rollback scripts and tasks for prod cases cause it is a pet project only.

2. The main class is DriverApplication. Active profile is dev. I have not made another profiles and configurations cause it is a pet project.

3. The Swagger api documentation is on http://localhost:8080/swagger-ui/index.html

There are some health check points from actuator.

4. On http://localhost:8080/ exposed refs for PagingAndSortingRepository. In the raw module there are some Postman scripts to test controllers
(use id for entities from your database).

5. Log files are logs/dev_app.log and logs/dev_app_test.log

6. Cache structure enable via ehcache.xml. I did not perform methods for cache evict, put and cache event listeners
cause it is a pet project only.

7. You can connect with JVisualVM. It is possible to expose EhCache MBeans through JMX and get overview the cache statistics.

8. jacocoTestReport task generates the test coverage report.

9. Some integration tests cover the main aspects of application.