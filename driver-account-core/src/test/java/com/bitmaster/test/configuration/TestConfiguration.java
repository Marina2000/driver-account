package com.bitmaster.test.configuration;

import com.bitmaster.edu.controller.ApiController;
import com.bitmaster.edu.domain.Account;
import com.bitmaster.edu.domain.Operation;
import com.bitmaster.edu.exception.ResourceNotFoundException;
import com.bitmaster.edu.service.AccountService;
import com.bitmaster.edu.service.OperationService;
import org.mockito.Mockito;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@TestPropertySource(locations = "/application.yaml")
@Configuration
@EnableCaching
public class TestConfiguration {
    @Bean
    OperationService operationService() {
        ArrayList<Operation> result = new ArrayList<Operation>();
        Operation operation = new Operation();
        operation.setAmount(1L);
        OperationService operationService = Mockito.mock(OperationService.class);
        result.add(operation);
        when(operationService.getCredit("1")).thenReturn(result);
        when(operationService.getCredit("2")).thenThrow(new ResourceNotFoundException());
        when(operationService.findByAccount(anyString())).thenReturn(Arrays.asList(operation));
        return operationService;
    }

    @Bean
    AccountService accountService() {
        return Mockito.mock(AccountService.class);
    }

    @Bean
    ApiController apiController() {
        ApiController apiController = new ApiController();
        apiController.setOperationService(operationService());
        apiController.setAccountService(accountService());
        return apiController;
    }

    @Bean
    CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("operation");
    }
}
