package com.bitmaster.test.integration;

import com.bitmaster.edu.DriverApplication;
import com.bitmaster.edu.domain.Account;
import com.bitmaster.edu.repository.AccountRepository;
import com.bitmaster.edu.service.AccountService;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@ExtendWith(SpringExtension.class)
@EntityScan(value = "com.bitmaster.edu.domain")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = {DriverApplication.class})
@EnableCaching
public class ApiControllerIntegrationTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    public AccountRepository accountRepository;

    @Autowired
    public AccountService accountService;

    @Autowired
    EhCacheCacheManager ehCacheCacheManager;

    @BeforeEach
    public void setup(){
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testPagingSort() throws Exception {
        mockMvc = webAppContextSetup(webApplicationContext).build();
        accountRepository.save(new Account());
        accountRepository.save(new Account());
        accountRepository.save(new Account());
        accountRepository.save(new Account());
        accountRepository.save(new Account());
        Matcher<String> driverMatcher = containsString("driver");
        Matcher<String> firstPageMatcher = containsString("http://localhost/accounts/?page=0&size=2&sort=id,desc&sort=amount,asc");
        this.mockMvc
                .perform(get("/accounts/?size=2&page=0&sort=id,desc&sort=amount").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(driverMatcher))
                .andExpect(MockMvcResultMatchers.content().string(firstPageMatcher));
    }

    @Test
    public void putOnAccountTest() throws Exception {
        Account savedAccount = accountRepository.save(new Account());
        String urlTemplate = "/putOnAccount?id="+savedAccount.getId()+"&sum=10";
        Matcher<String> idMatcher = containsString("id\":\""+savedAccount.getId());
        Matcher<String> amountMatcher = containsString("\"amount\":10");
        this.mockMvc
                .perform(post(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(idMatcher))
                .andExpect(MockMvcResultMatchers.content().string(amountMatcher));
    }

    @Test
    void removeFromAccountNegativeTest() throws Exception {
        Account account = new Account();
        account.setAmount(1);
        accountRepository.save(account);
        Matcher<String> errorMatcher = containsString("insufficient amount on account");
        String urlTemplate = "/removeFromAccount?id="+account.getId()+"&sum=10";
        this.mockMvc
                .perform(post(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(420))
        .andExpect(MockMvcResultMatchers.status().reason(errorMatcher));

    }

    @Test
    public void cacheTest(){
        Account savedAccount = accountRepository.save(new Account());
        accountService.getAccount(savedAccount.getId());
        accountRepository.deleteById(savedAccount.getId());
        Account deletedAccount = accountService.getAccount(savedAccount.getId());
        Assert.assertEquals(deletedAccount.getId(), savedAccount.getId());
        Assert.assertTrue(!accountRepository.findById(savedAccount.getId()).isPresent());
    }

    @Test
    public void cacheConfigurationTest(){
        Assert.assertNotNull(ehCacheCacheManager.getCache("account"));
    }
}
