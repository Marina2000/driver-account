package com.bitmaster.test.controller;

import com.bitmaster.edu.controller.ApiController;
import com.bitmaster.test.configuration.TestConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ApiController.class)
@ContextConfiguration(classes = TestConfiguration.class)

public class ApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getBalanceTest() throws Exception {
        this.mockMvc
                .perform(get("/getCredit?id=1").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[{\"id\":null,\"date\":null,\"amount\":1,\"account\":null}]"));
    }

    @Test
    void getBalanceExceptionTest() throws Exception {
        this.mockMvc
                .perform(get("/getCredit?id=2").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is(404));
    }
}
