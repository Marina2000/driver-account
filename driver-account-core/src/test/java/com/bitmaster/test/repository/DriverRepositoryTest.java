package com.bitmaster.test.repository;

import com.bitmaster.edu.domain.Driver;
import com.bitmaster.edu.repository.DriverRepository;
import com.bitmaster.test.configuration.TestConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@EntityScan(value = "com.bitmaster.edu.domain")
@EnableJpaRepositories({"com.bitmaster.edu.repository"})
@ContextConfiguration(classes = TestConfiguration.class)
public class DriverRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DriverRepository driverRepository;

    @Test
    public void testMainRepositoryOperations() throws Exception {
        Driver testDriver = new Driver();
        testDriver.setName("a");
        Driver result = driverRepository.save(testDriver);
        Driver find = entityManager.find(Driver.class, result.getId());
        Assert.assertTrue(find!=null);

        Driver driver = new Driver();
        driver.setName("aaa");
        entityManager.persist(driver);
        entityManager.flush();
        Assert.assertTrue(driverRepository.findByName("aaa").size()>0);
    }
}
