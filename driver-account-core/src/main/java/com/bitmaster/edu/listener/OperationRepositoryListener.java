package com.bitmaster.edu.listener;

import com.bitmaster.edu.domain.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler(Operation.class)
public class OperationRepositoryListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(OperationRepositoryListener.class);

    @HandleBeforeCreate
    public void handleBeforeSave(Operation operation) {
        LOGGER.info("Saving operation " + operation.toString());
    }

    @HandleAfterCreate
    public void handleAfterSave(Operation operation) {
        LOGGER.info("operation " + operation.toString() + " successfully created");
    }

    @HandleBeforeDelete
    public void handleBeforeDelete(Operation operation) {
        LOGGER.info("Deleting operation " + operation.toString());
    }

    @HandleAfterDelete
    public void handleAfterDelete(Operation operation) {
        LOGGER.info("operation " + operation.toString() + " successfully deleted");
    }
}
