package com.bitmaster.edu.listener;

import com.bitmaster.edu.domain.Driver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler(Driver.class)
public class DriverRepositoryListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(DriverRepositoryListener.class);

    @HandleBeforeCreate
    public void handleBeforeSave(Driver driver) {
        LOGGER.info("Saving driver " + driver.toString());
    }

    @HandleAfterCreate
    public void handleAfterSave(Driver driver) {
        LOGGER.info("driver " + driver.toString() + " successfully created");
    }

    @HandleBeforeDelete
    public void handleBeforeDelete(Driver driver) {
        LOGGER.info("Deleting driver " + driver.toString());
    }

    @HandleAfterDelete
    public void handleAfterDelete(Driver driver) {
        LOGGER.info("driver " + driver.toString() + " successfully deleted");
    }
}
