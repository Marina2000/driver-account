package com.bitmaster.edu.listener;

import com.bitmaster.edu.domain.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler(Account.class)
public class AccountRepositoryListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountRepositoryListener.class);

    @HandleBeforeCreate
    public void handleBeforeSave(Account account) {
        LOGGER.info("Saving account " + account.toString());
    }

    @HandleAfterCreate
    public void handleAfterSave(Account account) {
        LOGGER.info("account " + account.toString() + " successfully created");
    }

    @HandleBeforeDelete
    public void handleBeforeDelete(Account account) {
        LOGGER.info("Deleting account " + account.toString());
    }

    @HandleAfterDelete
    public void handleAfterDelete(Account account) {
        LOGGER.info("account " + account.toString() + " successfully deleted");
    }
}
