package com.bitmaster.edu.repository;

import com.bitmaster.edu.domain.Account;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccountRepository extends PagingAndSortingRepository<Account, String> {
}
