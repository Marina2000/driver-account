package com.bitmaster.edu.repository;

import com.bitmaster.edu.domain.Operation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.QueryHint;
import java.util.Date;
import java.util.List;

public interface OperationRepository extends PagingAndSortingRepository<Operation, String> {
    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    List<Operation> findByAccountId(String accountId);

    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    List<Operation> findByDate(Date date);

    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    @Query("select o from Operation o where o.date>= :start and o.date<= :finish")
    List<Operation> findPerPeriod(@Param("start") Date start, @Param("finish") Date finish);

    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    @Query("select o from Operation o where o.date>= :start and o.date<= :finish and o.amount>0")
    List<Operation> getDebitPerPeriod(@Param("start") Date start, @Param("finish") Date finish);

    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    @Query("select o from Operation o where o.date>= :start and o.date<= :finish and o.amount<0")
    List<Operation> getCreditPerPeriod(@Param("start") Date start, @Param("finish") Date finish);

    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    @Query("select o from Operation o where o.account.id= :accountId and o.amount>0")
    List<Operation> getDebit(@Param("accountId") String accountId);

    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    @Query("select o from Operation o where o.account.id= :accountId and o.amount<0")
    List<Operation> getCredit(@Param("accountId") String accountId);
}
