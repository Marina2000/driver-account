package com.bitmaster.edu.repository;

import com.bitmaster.edu.domain.Driver;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

public interface DriverRepository extends PagingAndSortingRepository<Driver, String> {
    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    List<Driver> findByName(@Param("name") String name);
}
