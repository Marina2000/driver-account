package com.bitmaster.edu.service.impl;

import com.bitmaster.edu.domain.Account;
import com.bitmaster.edu.domain.Operation;
import com.bitmaster.edu.exception.InsufficientAmountException;
import com.bitmaster.edu.exception.ResourceNotFoundException;
import com.bitmaster.edu.repository.AccountRepository;
import com.bitmaster.edu.repository.OperationRepository;
import com.bitmaster.edu.service.AccountService;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private OperationRepository operationRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Transactional
    @Override
    public Account putOnAccount(String id, long sum) {
        Optional<Account> optional = accountRepository.findById(id);
        if(!optional.isPresent()){
            throw new ResourceNotFoundException("there is no such account");
        }
        Account account = optional.get();
        Operation operation = new Operation();
        operation.setDate(new Date());
        operation.setAmount(sum);
        operation.setAccount(account);
        account.setAmount(account.getAmount()+sum);
        account.getOperations().add(operation);
        operationRepository.save(operation);
        accountRepository.save(account);
        return account;
    }

    @Transactional
    @Override
    public Account removeFromAccount(String id, long sum) throws InsufficientAmountException {
        Optional<Account> optional =  accountRepository.findById(id);
        if(!optional.isPresent()){
            throw new ResourceNotFoundException("there is no such account");
        }
        Account account = optional.get();
        Operation operation = new Operation();
        operation.setDate(new Date());
        operation.setAmount(sum);
        operation.setAccount(account);
        if(account.getAmount()-sum<0){
            throw new InsufficientAmountException("there is no enough money on account");
        }
        account.setAmount(account.getAmount()-sum);
        account.getOperations().add(operation);
        accountRepository.save(account);
        operationRepository.save(operation);
        return account;
    }

    @Transactional(readOnly = true)
    @Cacheable(cacheNames="account", key="#id")
    @Override
    public Account getAccount(String id) {
        Optional<Account> optional =  accountRepository.findById(id);
        if(!optional.isPresent()){
            throw new ResourceNotFoundException("there is no such account");
        }
        return optional.get();
    }

    @Transactional
    @Override
    public Pair<Account, Account> transfer(String fromId, String toId, long sum) {
        Optional<Account> optionalFrom =  accountRepository.findById(fromId);
        if(!optionalFrom.isPresent()){
            throw new ResourceNotFoundException("there is no such account");
        }
        Account from = optionalFrom.get();
        if(from.getAmount() - sum<0){
            throw  new InsufficientAmountException("there is no enough money on account");
        }
        Optional<Account> optionalTo =  accountRepository.findById(toId);
        if(!optionalTo.isPresent()){
            throw new ResourceNotFoundException("there is no such account");
        }
        Account to = optionalTo.get();
        Operation fromOperation = new Operation();
        Date current = new Date();
        fromOperation.setDate(current);
        fromOperation.setAccount(from);
        fromOperation.setAmount(-sum);
        from.setAmount(from.getAmount() - sum);
        from.getOperations().add(fromOperation);
        Operation toOperation = new Operation();
        toOperation.setDate(current);
        toOperation.setAccount(to);
        toOperation.setAmount(sum);
        to.setAmount(to.getAmount() + sum);
        to.getOperations().add(toOperation);
        accountRepository.save(from);
        operationRepository.save(fromOperation);
        accountRepository.save(to);
        operationRepository.save(toOperation);
        return new Pair<>(from, to);
    }
}
