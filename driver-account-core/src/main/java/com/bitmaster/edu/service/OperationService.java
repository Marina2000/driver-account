package com.bitmaster.edu.service;

import com.bitmaster.edu.domain.Operation;

import java.util.Date;
import java.util.List;

public interface OperationService {
    List<Operation> findByAccount(String accountId);

    List<Operation> findByDate(Date date);

    List<Operation> findPerPeriod(Date start, Date finish);

    List<Operation> getDebit(String accountId);

    List<Operation> getCredit(String accountId);

    List<Operation> getOperationList(String accountId);
}
