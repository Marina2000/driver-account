package com.bitmaster.edu.service;

import com.bitmaster.edu.domain.Account;
import com.bitmaster.edu.exception.InsufficientAmountException;
import javafx.util.Pair;

public interface AccountService {
    Account putOnAccount(String id, long sum);
    Account removeFromAccount(String id, long sum) throws InsufficientAmountException;
    Account getAccount(String id);
    Pair<Account, Account> transfer(String fromId, String toId, long sum);
}
