package com.bitmaster.edu.service.impl;

import com.bitmaster.edu.domain.Operation;
import com.bitmaster.edu.repository.OperationRepository;
import com.bitmaster.edu.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.awt.print.Pageable;
import java.util.Date;
import java.util.List;

@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class OperationServiceImpl implements OperationService {
    @Autowired
    private OperationRepository operationRepository;
    @Autowired
    CacheManager cacheManager;

    @Override
    public List<Operation> findByAccount(String accountId) {
        return operationRepository.findByAccountId(accountId);
    }

    @Override
    public List<Operation> findByDate(Date date) {
        return operationRepository.findByDate(date);
    }

    @Override
    public List<Operation> findPerPeriod(Date start, Date finish) {
        return operationRepository.findPerPeriod(start, finish);
    }

    @Override
    public List<Operation> getDebit(String accountId) {
        return operationRepository.getDebit(accountId);
    }

    @Override
    public List<Operation> getCredit(String accountId) {
        return operationRepository.getCredit(accountId);
    }

     @Override
     @Cacheable(cacheNames="operation", key="#accountId")
    public List<Operation> getOperationList(String accountId) {
        return operationRepository.findByAccountId(accountId);
    }

    public void setOperationRepository(OperationRepository operationRepository) {
        this.operationRepository = operationRepository;
    }
}
