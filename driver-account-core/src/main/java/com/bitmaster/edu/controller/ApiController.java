package com.bitmaster.edu.controller;

import com.bitmaster.edu.domain.Account;
import com.bitmaster.edu.domain.Operation;
import com.bitmaster.edu.service.AccountService;
import com.bitmaster.edu.service.OperationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully retrieved list"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
}
)
@Api(value = "online driver account", description = "Operations with driver's account")
@RestController
public class ApiController {

    @Autowired
    private OperationService operationService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(path = "/getBalance", method = RequestMethod.GET)
    public long getBalance(@RequestParam String id) {
        return accountService.getAccount(id).getAmount();
    }

    @RequestMapping(path = "/putOnAccount", method = RequestMethod.POST)
    public Account putOnAccount(@RequestParam String id, @RequestParam long sum) {
        return accountService.putOnAccount(id, sum);
    }

    @RequestMapping(path = "/removeFromAccount", method = RequestMethod.POST)
    public Account removeFromAccount(@RequestParam String id, @RequestParam long sum) {
        return accountService.removeFromAccount(id, sum);
    }

    @RequestMapping(path = "/transfer", method = RequestMethod.POST)
    public Pair<Account, Account> transfer(@RequestParam String fromId, @RequestParam String toId, @RequestParam long sum) {
        return accountService.transfer(fromId, toId, sum);
    }

    @RequestMapping(path = "/getDebit", method = RequestMethod.GET)
    public List<Operation> getDebet(@RequestParam String id) {
        return operationService.getDebit(id);
    }

    @RequestMapping(path = "/getCredit", method = RequestMethod.GET)
    public List<Operation> getCredit(@RequestParam String id) {
        return operationService.getCredit(id);
    }

    @RequestMapping(path = "/getOperationList", method = RequestMethod.GET)
    public List<Operation> getOperationList(@RequestParam String id) {
        return operationService.getOperationList(id);
    }

    public void setOperationService(OperationService operationService) {
        this.operationService = operationService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }
}
