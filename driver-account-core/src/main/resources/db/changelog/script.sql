create table driver
(id varchar(50) NOT NULL,
name varchar(20) NOT NULL,
surname varchar(20) NOT NULL,
middlename varchar(20) NOT NULL,
CONSTRAINT driver_pk PRIMARY KEY (id)
);

create table account
(id varchar(50) NOT NULL,
driver_id varchar(50),
amount integer,
CONSTRAINT id_pk PRIMARY KEY (id),
CONSTRAINT driver_account_id FOREIGN KEY (driver_id) REFERENCES driver (id)
);

create table operation
(id varchar(50) NOT NULL,
account_id varchar(50),
amount integer NOT NULL,
operation_date date NOT NULL,
CONSTRAINT operation_id_pk PRIMARY KEY (id),
CONSTRAINT account_operation_id FOREIGN KEY (account_id) REFERENCES account (id)
);

CREATE ROLE my_user2 LOGIN PASSWORD 'my_password';
GRANT ALL PRIVILEGES ON DATABASE "driver_account" to my_user2;
GRANT ALL PRIVILEGES ON TABLE "driver" to my_user2;
GRANT ALL PRIVILEGES ON TABLE "account" to my_user2;

